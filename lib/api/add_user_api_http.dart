import 'package:fitnessapp/api/api_demo_http.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddUserHttp extends StatefulWidget {
  Map? data;

  AddUserHttp(this.data);

  @override
  State<AddUserHttp> createState() => _AddUserHttpState();
}

class _AddUserHttpState extends State<AddUserHttp> {
  final formkey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController avtarController = TextEditingController();
  TextEditingController phController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    if (widget.data != null) {
      nameController =
          TextEditingController(text: widget.data!['name'].toString());
      avtarController =
          TextEditingController(text: widget.data!['avatar'].toString());
      phController =
          TextEditingController(text: widget.data!['phoneno'].toString());
      emailController =
          TextEditingController(text: widget.data!['email'].toString());
    }
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
            key: formkey,
            child: Column(
              children: [
                getFromField('Name', nameController, TextInputType.name),
                getFromField('Photo', avtarController, TextInputType.name),
                getFromField('Contact No.', phController, TextInputType.number),
                getFromField(
                    'Email', emailController, TextInputType.emailAddress),
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius:
                      const BorderRadius.all(Radius.circular(1000))
                    //more than 50% of width makes circle
                  ),
                  child: TextButton(
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      if (formkey.currentState!.validate()) {
                        if (widget.data == null) {
                          addUser()
                              .then((value) {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ApiDemoHttp(),));
                          });
                        }
                        else {
                          updateUser(widget.data!['id']).then((value) =>
                              Navigator.of(context).pop());
                        }
                      }
                    },
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getFromField(String title, control, keyboardType) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: TextFormField(
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Enter valid $title';
          }
        },
        controller: control,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: (title),
        ),
        style: TextStyle(
          color: Colors.black,
          fontSize: 15,
        ),
      ),
    );
  }

  Future<http.Response> addUser() async {
    Map map = {};
    map['name'] = nameController.text;
    map['avatar'] = avtarController.text;
    map['phoneno'] = phController.text;
    map['email'] = emailController.text;

    var res = await http.post(
        Uri.parse('https://630d987bb37c364eb706c169.mockapi.io/workers'),
        body: map);
    return res;
  }

  Future<http.Response> updateUser(id) async {
    Map map = {};
    map['name'] = nameController.text;
    map['avatar'] = avtarController.text;
    map['phoneno'] = phController.text;
    map['email'] = emailController.text;

    var res = await http.put(
        Uri.parse('https://630d987bb37c364eb706c169.mockapi.io/workers/$id'),
        body: map);
    return res;
  }
}
