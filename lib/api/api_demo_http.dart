import 'dart:convert';
import 'package:fitnessapp/api/add_user_api_http.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiDemoHttp extends StatefulWidget {
  const ApiDemoHttp({Key? key}) : super(key: key);

  @override
  State<ApiDemoHttp> createState() => _ApiDemoHttpState();
}

class _ApiDemoHttpState extends State<ApiDemoHttp> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => AddUserHttp(null),
                  ),
                );
              },
              child:
                  Padding(padding: EdgeInsets.all(10), child: Icon(Icons.add)),
            )
          ],
          title: Center(
            child: Container(
                margin: EdgeInsets.only(left: 100),
                child: Icon(Icons.api_rounded)),
          ),
        ),
        body: FutureBuilder<dynamic>(
          builder: (context, snapshot) {
            if (snapshot.data != null && snapshot.hasData) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    // color: index%2==0?Colors.red:Colors.green,
                    elevation: 10,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Center(
                            child: CircleAvatar(
                              radius: 80,
                              backgroundImage: NetworkImage(
                                  snapshot.data[index]['avatar']),
                            ),
                          ),
                          Text('Name : ${snapshot.data[index]['name']}',
                              style: TextStyle(fontSize: 24)),
                          Text(
                              'Contact Number : ${snapshot.data[index]['phoneno']}',
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.grey.shade500)),
                          Text(
                            'Email : ${snapshot.data[index]['email']}',
                            style: TextStyle(
                                fontSize: 15, color: Colors.grey.shade500),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => AddUserHttp(snapshot.data[index]),
                                      ),
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.update,
                                        color: Colors.blue,
                                        size: 24,
                                      ),
                                      Text('Update'),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    deleteUser(snapshot.data[index]['id'])
                                        .then(
                                      (value) => () {
                                        if (value == true) {
                                          setState(() {});
                                        }
                                      },
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.delete_rounded,
                                        color: Colors.red,
                                        size: 24,
                                      ),
                                      Text('Delete')
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
                itemCount: snapshot.data.length,
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
          future: callApi(),
        ),
      ),
    );
  }

  Future<dynamic> callApi() async {
    http.Response res = await http
        .get(Uri.parse('https://630d987bb37c364eb706c169.mockapi.io/workers'));
    dynamic data = jsonDecode(res.body.toString());
    return data;
  }

  Future<http.Response> deleteUser(id) async {
    var res = await http.delete(
        Uri.parse('https://630d987bb37c364eb706c169.mockapi.io/workers/$id'));
    return res;
  }
}