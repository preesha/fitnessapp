import 'package:flutter/material.dart';
import 'second.dart';
import 'third.dart';

class FirstPage extends StatefulWidget {
  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Padding(
            padding: EdgeInsets.all(6),
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          alignment: Alignment.centerLeft,
                          child: CircleAvatar(
                            backgroundColor: Colors.grey.shade200,
                            child: Icon(
                              Icons.dataset_rounded,
                              size: 30,
                              color: Colors.grey,
                            ),
                            radius: 24,
                          ),),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: const CircleAvatar(
                          radius: 24,
                          backgroundImage: AssetImage('assets/images/profile.png')
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, bottom: 7),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '   Hi,Sophie 👋',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 30,letterSpacing: 2),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10,bottom: 5,top: 30),
                        decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: TextField(
                          decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 17),
                            hintText: 'Search',
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.grey.shade600,
                            ),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(10),
                            suffixIcon: CircleAvatar(
                              backgroundColor: Colors.black,
                              child: Icon(
                                Icons.tune_rounded,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10, bottom: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Featured categories',
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 20),
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            getCategory('🧘', 'Yoga'),
                            getCategory('🏋️', 'Gym'),
                            getCategory('♥', 'Fitness'),
                            getCategory('🏃🏼‍♀', 'Run')
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10, bottom: 5),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Today plan',
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 20),
                          ),
                        ),
                        getPlan(
                            'assets/images/getPlan.jpeg',
                            '5 Km running',
                            '42 minutes',
                            110),
                        getPlan(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEhrXtRoK47Id2okCBSU201DNNKXkPSt-b1A&usqp=CAU',
                            'Yoga warm up',
                            '12 minutes',
                            150),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12,vertical: 10),
            // child: BottomNavigationBar(
            //   backgroundColor: Colors.black,
            //   items: [
            //     BottomNavigationBarItem(
            //       icon: Icon(Icons.home),
            //       label: 'Home',
            //       backgroundColor: Colors.black,
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(Icons.calendar_month),
            //       label: 'Schedule',
            //       backgroundColor: Colors.black54,
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(Icons.incomplete_circle),
            //       label: 'Progress',
            //       backgroundColor: Colors.black54,
            //     ),
            //     BottomNavigationBarItem(
            //       icon: Icon(Icons.settings),
            //       label: 'Settings',
            //       backgroundColor: Colors.black54,
            //     ),
            //   ],
            //   selectedItemColor: Colors.white,
            //   onTap: (index) {
            //     setState(() {
            //       selectedIndex = index;
            //     });
            //   },
            // ),
          ),
        )
    );
  }

  Widget getCategory(String title, String subTitle) {
    return Container(
      margin: EdgeInsets.only(left: 15),
      child: Column(
        children: [
          CircleAvatar(
            backgroundColor: Colors.grey.shade300,
            child: Text(
              title,
              style: TextStyle(fontSize: 25),
            ),
            radius: 35,
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            child: Text(
              subTitle,
              style: TextStyle(
                  fontSize: 15,
                  color: Colors.grey.shade600,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
  }

  Widget getPlan(String img, String title, String time, double width) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
      child: Card(
        elevation: 10,
        child: Padding(
          padding: EdgeInsets.all(5),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                width: 100,
                height: 100,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Image.network(img),
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Text(title,
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w700))),
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(
                        time,
                        style: TextStyle(fontWeight: FontWeight.w200),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      height: 4,
                      width: width,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(10)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
