import 'package:fitnessapp/api/api_demo_http.dart';
import 'package:fitnessapp/second.dart';
import 'package:fitnessapp/third.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'first.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> screen = [
    FirstPage(),
    ScheduleScreen(),
    ProgressScreen(),
    ApiDemoHttp()
  ];
  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: Scaffold(
          bottomNavigationBar: FloatingNavbar(
            backgroundColor: Colors.black,
            items: [
              FloatingNavbarItem(icon: Icons.home, title: 'Home'),
              FloatingNavbarItem(icon: Icons.calendar_month, title: 'Schedule'),
              FloatingNavbarItem(
                  icon: Icons.incomplete_circle_sharp, title: 'Progress'),
              FloatingNavbarItem(icon: Icons.api_rounded, title: 'API'),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
          ),
          body: Center(
            child: screen.elementAt(_selectedIndex),
          ),
        ));
  }
}
