import 'package:flutter/material.dart';

class ScheduleScreen extends StatefulWidget {
  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      // appBar: ,
      body: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        image: DecorationImage(
                          image: AssetImage('assets/images/stretching.jpeg'),
                          fit: BoxFit.cover,
                        )),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 100.0, top: 10),
                          child: Container(
                            child: Text(
                              'Stretching',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Column(
                    children: [
                      Expanded(
                          child: Row(
                        children: [
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 40.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Colors.grey.shade800,
                                  ),
                                  child: Column(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        child: Center(
                                            child: Padding(
                                          padding: const EdgeInsets.only(top: 20),
                                          child: Text(
                                            '🔥',
                                            style: TextStyle(fontSize: 25),
                                          ),
                                        )),
                                      )),
                                      Expanded(
                                          child: Container(
                                        child: Center(
                                            child: Text(
                                          '280',
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 35),
                                        )),
                                      )),
                                      Expanded(
                                          child: Container(
                                        child: Center(
                                            child: Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 40),
                                          child: Text(
                                            'calories',
                                            style: TextStyle(
                                                color: Colors.grey, fontSize: 10),
                                          ),
                                        )),
                                      ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )),
                          Expanded(
                              flex: 2,
                              child: Center(
                                  child: Padding(
                                padding: const EdgeInsets.only(top: 10,bottom: 10,right: 45),
                                child: Column(
                                  children: [
                                    Expanded(
                                      flex: 10,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(vertical: 5,horizontal: 35),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.grey.shade800,
                                      ),
                                      child: SizedBox(
                                        height: 100,
                                        width: 300,
                                        child: Center(
                                          child: Text(
                                            '⏰  42  min',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(horizontal: 25),
                                          decoration: BoxDecoration(
                                            borderRadius:BorderRadius.circular(12),
                                            color: Colors.grey.shade800,
                                          ),
                                          child: Expanded(
                                            //flex: 2,
                                            child: Column(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(top: 30,right: 55, bottom: 10),
                                                  child: Container(
                                                    child: Text(
                                                      'Focus Zone',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12,),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(
                                                        bottom: 20, top: 5, left: 8,right: 50),
                                                    child: Text(
                                                      'Full body',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 20),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )))
                        ],
                      ))
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    width: 350,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.redAccent,
                    ),
                    child: Center(
                      child: Text(
                        'Start Training',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
