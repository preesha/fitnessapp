import 'package:flutter/material.dart';

class ProgressScreen extends StatefulWidget {
  const ProgressScreen({Key? key}) : super(key: key);

  @override
  State<ProgressScreen> createState() => _ProgressScreenState();
}

class _ProgressScreenState extends State<ProgressScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.pink[50],
      // appBar: AppBar(
      //   elevation: 0,
      //   backgroundColor: Colors.pink[50],
      //   foregroundColor: Colors.white,
      //   leading: Padding(
      //     padding: const EdgeInsets.only(left: 30),
      //     child: Icon(
      //       Icons.arrow_back_ios_new,
      //       color: Colors.black,
      //     ),
      //   ),
      //   actions: [
      //     Padding(
      //       padding: const EdgeInsets.only(right: 30),
      //       child: Icon(
      //         Icons.more_horiz_sharp,
      //         color: Colors.black,
      //       ),
      //     )
      //   ],
      // ),

      body: Column(
        children: [
          Expanded(
              child: Column(
            children: [
              Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Expanded(
                          child: Container(
                        child: const CircleAvatar(
                          radius: 70,
                          backgroundImage: AssetImage('assets/images/profile.png')
                        ),
                      ))
                    ],
                  )),
              Expanded(
                  child: Column(
                children: [
                  Expanded(
                      child: Center(
                          child: Column(
                    children: [
                      Expanded(
                          child: Center(
                              child: Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Container(
                          child: Text('Sophie Wilson',
                              style: TextStyle(
                                  letterSpacing: 3,
                                  color: Colors.black,
                                  fontSize: 30)),
                        ),
                      )))
                    ],
                  ))),
                  Expanded(
                      child: Center(
                          child: Column(
                    children: [
                      Expanded(
                          child: Center(
                              child: Padding(
                        padding: const EdgeInsets.only(top: 7, bottom: 15),
                        child: Container(
                          child: Text(
                            '23 years old, Female',
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                          ),
                        ),
                      )))
                    ],
                  ))),
                ],
              )),
              Expanded(
                  child: Stack(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 30, right: 40),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 20, left: 20),
                          child: Text(
                            'Daily',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 30, left: 50),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.black,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 20, left: 20),
                          child: Text(
                            'Weekly',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Image.asset('assets/images/YourActivity.png'),
              ))
            ],
          ))
        ],
      ),
    ));
  }
}
